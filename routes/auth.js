var express = require('express');
var router  = express.Router();
var jwt     = require('jsonwebtoken');
var config  = require('../config/config');

var User    = require('../models/user');

router.post('/register', function(req, res) {
    if(!req.body.email || !req.body.username || !req.body.password) {
        console.log('Email, username or password is missing');
        return res.status(403).json({
            success: false,
            code: 403,
            data: {
                path: req.route.path,
                url: req.originalUrl,
                method: req.method,
                message: 'Email, username or password is missing'
            }
        });
    }

    User.create(req.body, function(err, registeredUser) {
        if(err) {
            console.log('Request error');
            return res.status(403).json({
                success: false,
                code: 403,
                data: {
                    path: req.route.path,
                    url: req.originalUrl,
                    method: req.method,
                    message: err.message
                }
            });
        }

        console.log('User registered');
        res.status(201).json({
            success: true,
            code: 201,
            data: registeredUser
        });
    });
});

router.post('/authenticate', function(req, res) {

    if(!req.body.username || !req.body.password) {
        console.log('Username or password is missing');
        return res.status(403).json({
            success: false,
            code: 403,
            data: {
                path: req.route.path,
                url: req.originalUrl,
                method: req.method,
                message: 'Username or password is missing'
            }
        });
    }

    User.findOne({ username: req.body.username }, function(err, user) {
        if(err) {
            console.log('Request error');
            return res.status(403).json({
                success: false,
                code: 403,
                data: {
                    path: req.route.path,
                    url: req.originalUrl,
                    method: req.method,
                    message: err.message
                }
            });
        }

        if(!user) {
            console.log(req.body.username + ' not found');
            return res.status(404).json({
                success: false,
                code: 404,
                data: {
                    path: req.route.path,
                    url: req.originalUrl,
                    method: req.method,
                    message: req.body.username + ' not found'
                }
            });
        }

        user.comparePassword(req.body.password, function(err, isMatch) {
            if(err) {
                console.log('Compare password failed');
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: 'Compare password failed'
                    }
                });
            }

            if(isMatch) {

                var token = jwt.sign({id: user._id, username: user.username}, config.secret, {
                    expiresIn: 10080
                });

                console.log('Authentication passed');
                res.status(200).json({
                    success: true,
                    code: 200,
                    data: {
                        token: 'JWT ' + token
                    }
                });

            } else {

                console.log('Authentication failed');
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: 'Authentication failed'
                    }
                });

            }
        })
    });
});

module.exports = router;
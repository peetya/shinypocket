(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .config(stateConfig);

    stateConfig.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];

    function stateConfig($urlRouterProvider, $locationProvider, $stateProvider) {

        //$urlRouterProvider.otherwise("/404");
        $locationProvider.html5Mode(true);

    }

})();
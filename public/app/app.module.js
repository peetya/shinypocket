(function() {
    'use strict';

    angular.module('shinypocket', [
        'shinypocket.core',
        'shinypocket.error',
        'shinypocket.login',
        'shinypocket.signup',
        'shinypocket.home',
        'shinypocket.cashflow',
        'shinypocket.account',
        'shinypocket.category',
        'shinypocket.report',
    ]);
})();
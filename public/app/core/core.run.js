(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .run(appRun);

    appRun.$inject = ['$rootScope', '$state', '$uibModalStack', 'authService', 'amMoment', 'logger'];

    function appRun($rootScope, $state, $uibModalStack, authService, amMoment, logger) {

        var vm = this;

        //amMoment.changeLocale('hu');

        $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState) {

            $uibModalStack.dismissAll();

            if (!authService.isAuthenticated()) {

                if (next.authenticate == true) {
                    event.preventDefault();
                    $state.go('login');
                    logger.warning("Access denied. Please log in.");
                }

            } else {

                if (next.name == 'login' || next.name == 'signup') {
                    event.preventDefault();
                    $state.go('home');
                }

            }

        });

    }
})();
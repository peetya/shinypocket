(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('reportService', reportService);

    function reportService($resource) {

        return {
            reports: serviceReports,
            charts: serviceCharts
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceReports() {
            return $resource('/api/v1/reports/:year/:month', { year: 'year', month: '@month' });
        }

        function serviceCharts() {
            return $resource('api/v1/charts/:type/:year/:month', { type: 'type', year: '@year', month: '@month'});
        }
    }

})();
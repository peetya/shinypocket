var express = require('express');
var router  = express.Router();
var jwt     = require('jsonwebtoken');

var config  = require('../config/config');
var User    = require('../models/user');
var Verify  = require('../helpers/verify');

router.route('/me')
    .get(Verify.verifyUser, function(req, res) {

        User.findById(req.decoded.id, {password: 0}, function(err, user) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!user) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.decoded.username + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: user
            });
        });

    });

    // TODO: Hash password doesn't work in case of update
    //.put(Verify.verifyUser, function(req, res) {
    //
    //    User.findByIdAndUpdate(req.decoded.id, { $set: req.body }, function(err, user) {
    //        if(err) {
    //            return res.status(403).json({
    //                success: false,
    //                code: 403,
    //                data: {
    //                    path: req.route.path,
    //                    url: req.originalUrl,
    //                    method: req.method,
    //                    message: 'Request error'
    //                }
    //            });
    //        }
    //
    //        if(!user) {
    //            return res.status(404).json({
    //                success: false,
    //                code: 404,
    //                data: {
    //                    path: req.route.path,
    //                    url: req.originalUrl,
    //                    method: req.method,
    //                    message: req.decoded.username + ' not found'
    //                }
    //            });
    //        }
    //
    //        res.status(200).json({
    //            success: true,
    //            code: 200,
    //            data: user
    //        });
    //    });
    //
    //});

module.exports = router;
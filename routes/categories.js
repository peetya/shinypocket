var express  = require('express');
var router   = express.Router();
var jwt      = require('jsonwebtoken');

var config   = require('../config/config');
var Category = require('../models/category');
var Verify   = require('../helpers/verify');

router.route('/')
    .get(Verify.verifyUser, function(req, res) {

        Category.find({ user: req.decoded.id }, function(err, categories) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!categories) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: 'Not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: categories
            });
        });

    })

    .post(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Category.create(req.body, function(err, createdCategory) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            var id = createdCategory._id;

            Category.findById(id, function(err, category) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                if(!category) {
                    return res.status(404).json({
                        success: false,
                        code: 404,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: id + ' not found'
                        }
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 201,
                    data: category
                });
            });

        });

    });

router.route('/:id')
    .get(Verify.verifyUser, function(req, res) {

        Category.findById(req.params.id, function(err, category) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!category) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: category
            });
        });

    })

    .put(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Category.findByIdAndUpdate(req.params.id, { $set: req.body }, function(err, category) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!category) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: category
            });
        });

    })

    .delete(Verify.verifyUser, function(req, res) {

        Category.findOneAndRemove({ _id: req.params.id, user: req.decoded.id }, function(err, category) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!category) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: category
            });
        });

    });

module.exports = router;
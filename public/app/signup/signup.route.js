(function() {
    'use strict';

    angular
        .module('shinypocket.signup')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('signup', {
                url: "/signup",
                views: {
                    'content': { templateUrl: "app/signup/signup.template.html", controller: "SignupController as vm" }
                }
            })

    }

})();
(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .controller('AccountController', AccountController);

    AccountController.$inject = ['$q', '$state', '$uibModal', 'logger', 'accountService'];

    function AccountController($q, $state, $uibModal, logger, accountService) {

        var vm = this;

        vm.accounts   = {};
        vm.pagination = {};

        vm.openAccountDeleteModal = openAccountDeleteModal;

        activate();

        function activate() {

            var promises = [getAccounts()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function getAccounts() {
            var deferrer = $q.defer();

            accountService.accounts().get(function(accounts) {

                deferrer.resolve();
                vm.accounts   = accounts.data;
                vm.pagination = accounts.pagination;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function openAccountDeleteModal(accountId) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/account/account.delete.modal.template.html',
                controller: 'AccountDeleteController as vm',
                resolve: {
                    param: function() {
                        return { accountId: accountId };
                    }
                }
            });
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .controller('CategoryDeleteController', CategoryDeleteController);

    CategoryDeleteController.$inject = ['$q', '$state', '$stateParams', 'logger', 'categoryService', 'param'];

    function CategoryDeleteController($q, $state, $stateParams, logger, categoryService, param) {

        var vm = this;

        vm.category  = {};

        vm.deleteCategory = deleteCategory;

        ////////// Controller functions //////////

        function deleteCategory() {
            categoryService.categories().remove({id: param.categoryId}, function(category) {

                logger.success('Category successfully removed!');
                $state.go('category', {}, {reload: true});

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .controller('AccountDeleteController', AccountDeleteController);

    AccountDeleteController.$inject = ['$q', '$state', '$stateParams', 'logger', 'accountService', 'param'];

    function AccountDeleteController($q, $state, $stateParams, logger, accountService, param) {

        var vm = this;

        vm.account  = {};

        vm.deleteAccount = deleteAccount;

        ////////// Controller functions //////////

        function deleteAccount() {
            accountService.accounts().remove({id: param.accountId}, function(account) {

                logger.success('Account successfully removed!');
                $state.go('account', {}, {reload: true});

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

    }
})();
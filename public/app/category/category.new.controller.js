(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .controller('CategoryNewController', CategoryNewController);

    CategoryNewController.$inject = ['$state', 'logger', 'categoryService', 'iconService'];

    function CategoryNewController($state, logger, categoryService, iconService) {

        var vm   = this;

        vm.category  = {};
        vm.title    = 'Add Category';
        vm.icons    = iconService.icons;

        vm.save      = save;

        ////////// Controller functions //////////

        function save() {
            categoryService.categories().save(vm.category, function(category) {

                logger.success('Category successfully created!');
                $state.go('category');

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('shinypocket.report')
        .controller('ReportYearlyChartController', ReportYearlyChartController);

    ReportYearlyChartController.$inject = ['$q', '$stateParams', '$state', 'logger', 'reportService'];

    function ReportYearlyChartController($q, $stateParams, $state, logger, reportService) {

        var vm = this;

        vm.data    = {};
        vm.options = {};

        vm.details = {
            year: parseInt($stateParams.year),
            nextYear: 0,
            prevYear: 0
        };

        vm.options.chart = {
            type: 'pieChart',
            height: 500,
            x: function(d){return d._id.name;},
            y: function(d){
                for(var i = 0; i < d.details.length; i++) {
                    if(d.details[i].type == 'balance') { return d.details[i].totalAmount; }
                }
                return 0;
            },
            showLabels: true,
            duration: 500,
            labelThreshold: 0.01,
            labelSunbeamLayout: true
        };

        activate();

        function activate() {

            vm.details.nextYear = (vm.details.nextMonth - vm.details.month != 1 ? vm.details.year + 1 : vm.details.year);
            vm.details.prevYear = (vm.details.month - vm.details.prevMonth != 1 ? vm.details.year - 1 : vm.details.year);

            var promises = [getAcountChartReportData(), getCategoryChartReportData()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////


        function getAcountChartReportData() {
            var deferrer = $q.defer();

            reportService.charts().get({ type: 'accounts', year: $stateParams.year }, function(data) {

                deferrer.resolve();
                vm.data.accounts = data.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function getCategoryChartReportData() {
            var deferrer = $q.defer();

            reportService.charts().get({ type: 'categories', year: $stateParams.year }, function(data) {

                deferrer.resolve();
                vm.data.categories = data.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

    }
})();
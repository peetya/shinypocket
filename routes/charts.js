var express  = require('express');
var router   = express.Router();
var jwt      = require('jsonwebtoken');
var mongoose = require('mongoose');

var config   = require('../config/config');
var Cashflow = require('../models/cashflow');
var User     = require('../models/user');
var Verify   = require('../helpers/verify');

router.route('/categories/:year/:month')
    .get(Verify.verifyUser, function(req, res) {

        var currentMonth = "0" + req.params.month;
        var nextMonth    = "0" + (parseInt(req.params.month) + 1).toString();
        var nextYear     = req.params.year;

        // TODO: ide valami szebb megoldás kéne :)
        if(nextMonth == '013') {
            nextYear  = (parseInt(nextYear) + 1).toString();
            nextMonth = '01';
        }

        var dateStart    = new Date(req.params.year + "-" + currentMonth.substr(currentMonth.length - 2) + "-01T00:00:00.000Z");
        var dateEnd      = new Date(nextYear + "-" + nextMonth.substr(nextMonth.length - 2) + "-01T00:00:00.000Z");

        Cashflow.aggregate([
            {
                $match: {
                    $and: [
                        { user: new mongoose.Types.ObjectId(req.decoded.id) },
                        { date: { "$gte": dateStart, "$lt": dateEnd } }
                    ]
                }
            },
            {
                $group: {
                    _id: {
                        category: "$category",
                        type: "$type"
                    },
                    totalAmount: {
                        $sum: {
                            $multiply: ["$amount"]
                        }
                    },
                    total: { $sum: 1 }
                },
            },
            {
                $group: {
                    _id: "$_id.category",
                    details: {

                        $push: {
                            "type": "$_id.type",
                            "totalAmount": "$totalAmount",
                            "numberOfItems": "$total"
                        }

                    }
                }
            }
        ], function(err, result) {
            Cashflow.populate(result, {path: "_id", model: "Category"}, function(err, results) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                // Summation of the income and the expense
                for(var j = 0; j < results.length; j++) {
                    var sum = 0;
                    var numOfItems = 0;
                    for(var i = 0; i < results[j].details.length; i++) {
                        if (results[j].details[i].type == 'income')  sum += results[j].details[i].totalAmount;
                        if (results[j].details[i].type == 'expense') sum -= results[j].details[i].totalAmount;
                        numOfItems += results[j].details[i].numberOfItems;
                    }
                    results[j].details.push({
                        'type': 'balance',
                        'totalAmount': sum,
                        'numberOfItems': numOfItems
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: results
                });
            });

        });

    });

router.route('/accounts/:year/:month')
    .get(Verify.verifyUser, function(req, res) {

        var currentMonth = "0" + req.params.month;
        var nextMonth    = "0" + (parseInt(req.params.month) + 1).toString();
        var nextYear     = req.params.year;

        // TODO: ide valami szebb megoldás kéne :)
        if(nextMonth == '013') {
            nextYear  = (parseInt(nextYear) + 1).toString();
            nextMonth = '01';
        }

        var dateStart    = new Date(req.params.year + "-" + currentMonth.substr(currentMonth.length - 2) + "-01T00:00:00.000Z");
        var dateEnd      = new Date(nextYear + "-" + nextMonth.substr(nextMonth.length - 2) + "-01T00:00:00.000Z");

        Cashflow.aggregate([
            {
                $match: {
                    $and: [
                        { user: new mongoose.Types.ObjectId(req.decoded.id) },
                        { date: { "$gte": dateStart, "$lt": dateEnd } }
                    ]
                }
            },
            {
                $group: {
                    _id: {
                        account: "$account",
                        type: "$type"
                    },
                    totalAmount: {
                        $sum: {
                            $multiply: ["$amount"]
                        }
                    },
                    total: { $sum: 1 }
                },
            },
            {
                $group: {
                    _id: "$_id.account",
                    details: {

                        $push: {
                            "type": "$_id.type",
                            "totalAmount": "$totalAmount",
                            "numberOfItems": "$total"
                        }

                    }
                }
            }
        ], function(err, result) {
            Cashflow.populate(result, {path: "_id", model: "Account"}, function(err, results) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                 //Summation of the income and the expense
                for(var j = 0; j < results.length; j++) {
                    var sum = 0;
                    var numOfItems = 0;
                    for(var i = 0; i < results[j].details.length; i++) {
                        if (results[j].details[i].type == 'income')  sum += results[j].details[i].totalAmount;
                        if (results[j].details[i].type == 'expense') sum -= results[j].details[i].totalAmount;
                        numOfItems += results[j].details[i].numberOfItems;
                    }
                    results[j].details.push({
                        'type': 'balance',
                        'totalAmount': sum,
                        'numberOfItems': numOfItems
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: results
                });
            });

        });

    });

router.route('/categories/:year')
    .get(Verify.verifyUser, function(req, res) {

        var currentYear  = req.params.year;
        var nextYear     = (parseInt(currentYear) + 1).toString();

        var dateStart    = new Date(currentYear + "-01-01T00:00:00.000Z");
        var dateEnd      = new Date(nextYear    + "-01-01T00:00:00.000Z");

        Cashflow.aggregate([
            {
                $match: {
                    $and: [
                        { user: new mongoose.Types.ObjectId(req.decoded.id) },
                        { date: { "$gte": dateStart, "$lt": dateEnd } }
                    ]
                }
            },
            {
                $group: {
                    _id: {
                        category: "$category",
                        type: "$type"
                    },
                    totalAmount: {
                        $sum: {
                            $multiply: ["$amount"]
                        }
                    },
                    total: { $sum: 1 }
                },
            },
            {
                $group: {
                    _id: "$_id.category",
                    details: {

                        $push: {
                            "type": "$_id.type",
                            "totalAmount": "$totalAmount",
                            "numberOfItems": "$total"
                        }

                    }
                }
            }
        ], function(err, result) {
            Cashflow.populate(result, {path: "_id", model: "Category"}, function(err, results) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                // Summation of the income and the expense
                for(var j = 0; j < results.length; j++) {
                    var sum = 0;
                    var numOfItems = 0;
                    for(var i = 0; i < results[j].details.length; i++) {
                        if (results[j].details[i].type == 'income')  sum += results[j].details[i].totalAmount;
                        if (results[j].details[i].type == 'expense') sum -= results[j].details[i].totalAmount;
                        numOfItems += results[j].details[i].numberOfItems;
                    }
                    results[j].details.push({
                        'type': 'balance',
                        'totalAmount': sum,
                        'numberOfItems': numOfItems
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: results
                });
            });

        });

    });

router.route('/accounts/:year')
    .get(Verify.verifyUser, function(req, res) {

        var currentYear  = req.params.year;
        var nextYear     = (parseInt(currentYear) + 1).toString();

        var dateStart    = new Date(currentYear + "-01-01T00:00:00.000Z");
        var dateEnd      = new Date(nextYear    + "-01-01T00:00:00.000Z");

        Cashflow.aggregate([
            {
                $match: {
                    $and: [
                        { user: new mongoose.Types.ObjectId(req.decoded.id) },
                        { date: { "$gte": dateStart, "$lt": dateEnd } }
                    ]
                }
            },
            {
                $group: {
                    _id: {
                        account: "$account",
                        type: "$type"
                    },
                    totalAmount: {
                        $sum: {
                            $multiply: ["$amount"]
                        }
                    },
                    total: { $sum: 1 }
                },
            },
            {
                $group: {
                    _id: "$_id.account",
                    details: {

                        $push: {
                            "type": "$_id.type",
                            "totalAmount": "$totalAmount",
                            "numberOfItems": "$total"
                        }

                    }
                }
            }
        ], function(err, result) {
            Cashflow.populate(result, {path: "_id", model: "Account"}, function(err, results) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                //Summation of the income and the expense
                for(var j = 0; j < results.length; j++) {
                    var sum = 0;
                    var numOfItems = 0;
                    for(var i = 0; i < results[j].details.length; i++) {
                        if (results[j].details[i].type == 'income')  sum += results[j].details[i].totalAmount;
                        if (results[j].details[i].type == 'expense') sum -= results[j].details[i].totalAmount;
                        numOfItems += results[j].details[i].numberOfItems;
                    }
                    results[j].details.push({
                        'type': 'balance',
                        'totalAmount': sum,
                        'numberOfItems': numOfItems
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: results
                });
            });

        });

    });

router.route('/cashflow')
    .get(Verify.verifyUser, function(req, res) {

        var currentYear  = new Date().getFullYear();
        var currentMonth = new Date().getMonth() + 1;
        var currentDay   = new Date().getDate();

        if(currentMonth < 10) currentMonth = '0' + currentMonth;
        if(currentDay < 10)   currentDay   = '0' + currentDay;

        var previousYear = (parseInt(currentYear) - 1).toString();

        var dateStart    = new Date(previousYear + "-" + currentMonth + "-" + currentDay +"T00:00:00.000Z");
        var dateEnd      = new Date(currentYear  + "-" + currentMonth + "-" + currentDay +"T00:00:00.000Z");

        Cashflow.aggregate([
            {
                $match: {
                    $and: [
                        { user: new mongoose.Types.ObjectId(req.decoded.id) },
                        { date: { "$gte": dateStart, "$lt": dateEnd } }
                    ]
                }
            },
            {
                $sort: { date: 1 }
            },
            {
                $group: {
                    _id: { year: { $year: "$date" }, month: { $month: "$date" } },
                    data: {
                        $push: {
                            "type": "$type",
                            "amount": "$amount"
                        }
                    }
                }
            },
            {
                $unwind: "$data"
            }
            //{
            //    $group: {
            //        _id: {
            //            type: "$type"
            //        },
            //        totalAmount: {
            //            $sum: {
            //                $multiply: ["$amount"]
            //            }
            //        },
            //        total: { $sum: 1 }
            //    },
            //},
            //{
            //    $group: {
            //        _id: "$_id.type",
            //        details: {
            //
            //            $push: {
            //                "totalAmount": "$totalAmount",
            //                "numberOfItems": "$total"
            //            }
            //
            //        }
            //    }
            //}
        ], function(err, result) {

            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            ////Summation of the income and the expense
            //var sum = 0;
            //var numOfItems = 0;
            //
            //for(var j = 0; j < result.length; j++) {
            //
            //    if (result[j]._id == 'income')  sum += result[j].details[0].totalAmount;
            //    if (result[j]._id == 'expense') sum -= result[j].details[0].totalAmount;
            //    numOfItems += result[j].details[0].numberOfItems;
            //
            //}
            //
            //result.push({
            //    '_id': 'balance',
            //    'details': [{
            //        'totalAmount':   sum,
            //        'numberOfItems': numOfItems
            //    }]
            //});

            res.status(200).json({
                success: true,
                code: 200,
                data: result
            });

        });

    });

module.exports = router;
(function () {
    'use strict';

    angular
        .module('shinypocket')
        .service('authService', authService);

    authService.$inject = ['$http', 'jwtHelper'];

    function authService($http, jwtHelper) {

        var localTokenKey = 'authToken';

        return {
            isAuthenticated:        isAuthenticated,
            loadUserCredentials:    loadUserCredentials,
            storeUserCredentials:   storeUserCredentials,
            destroyUserCredentials: destroyUserCredentials,
            getToken:               getToken

        };

        //////////////////////////
        // Services
        //////////////////////////
        function isAuthenticated() {
            var token = window.localStorage.getItem(localTokenKey);

            if (token) {
                if (jwtHelper.isTokenExpired(token)) {
                    destroyUserCredentials();
                    return false;
                } else {
                    loadUserCredentials();
                    return true;
                }
            } else {
                destroyUserCredentials();
                return false;
            }
        }

        function loadUserCredentials() {
            var token = window.localStorage.getItem(localTokenKey);
            if (token) {
                useCredentials(token);
            }
        }

        function storeUserCredentials(token) {
            window.localStorage.setItem(localTokenKey, token);
        }

        function destroyUserCredentials() {
            $http.defaults.headers.common.Authorization = undefined;
            window.localStorage.removeItem(localTokenKey);
        }

        function useCredentials(token) {
            $http.defaults.headers.common.Authorization = token;
        }

        function getToken() {
            return window.localStorage.getItem(localTokenKey);
        }
    }

})();
(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .controller('CategoryController', CategoryController);

    CategoryController.$inject = ['$q', '$state', '$uibModal', 'logger', 'categoryService'];

    function CategoryController($q, $state, $uibModal, logger, categoryService) {

        var vm = this;

        vm.categories = {};
        vm.pagination = {};

        vm.openCategoryDeleteModal = openCategoryDeleteModal;

        activate();

        function activate() {

            var promises = [getCategories()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function getCategories() {
            var deferrer = $q.defer();

            categoryService.categories().get(function(categories) {

                deferrer.resolve();
                vm.categories   = categories.data;
                vm.pagination = categories.pagination;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function openCategoryDeleteModal(categoryId) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/category/category.delete.modal.template.html',
                controller: 'CategoryDeleteController as vm',
                resolve: {
                    param: function() {
                        return { categoryId: categoryId };
                    }
                }
            });
        }

    }
})();
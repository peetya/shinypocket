(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .constant('angularMomentConfig', {
            preprocess: 'utc',
            timezone: 'Europe/Berlin'
        });
})();
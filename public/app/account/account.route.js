(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('account', {
                url: "/my-accounts",
                authenticate: true,
                views: {
                    'content': { templateUrl: "app/account/account.template.html", controller: "AccountController as vm" }
                }
            })

            .state('account.new', {
                url: "/new",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/account/account.form.template.html", controller: "AccountNewController as vm" }
                }
            })

            .state('account.edit', {
                url: "/edit/:id",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/account/account.form.template.html", controller: "AccountEditController as vm" }
                }
            })

    }

})();
(function() {
    'use strict';

    angular
        .module('shinypocket.cashflow')
        .controller('IncomeController', IncomeController);

    IncomeController.$inject = ['$q', '$state', '$uibModalInstance', 'logger', 'categoryService', 'accountService', 'cashflowService'];

    function IncomeController($q, $state, $uibModalInstance, logger, categoryService, accountService, cashflowService) {

        var vm = this;

        vm.modal      = {};
        vm.accounts   = {};
        vm.categories = {};
        vm.cashflow   = {};

        vm.save = save;

        activate();

        function activate() {

            vm.modal = {
                title: 'Add income',
                background: 'bg-success'
            };

            var promises = [getCategories(), getAccounts()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function save() {
            vm.cashflow.type = 'income';
            cashflowService.cashflows().save(vm.cashflow, function(cashflow) {
                logger.success("Expense added!");
                $uibModalInstance.close();
                $state.go($state.current, {}, {reload: true});
            }, function(error) {
                logger.error(error.data.data.message);
            });
        }

        function getCategories() {
            var deferrer = $q.defer();

            categoryService.categories().get(function(categories) {

                deferrer.resolve();
                vm.categories = categories.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function getAccounts() {
            var deferrer = $q.defer();

            accountService.accounts().get(function(accounts) {

                deferrer.resolve();
                vm.accounts = accounts.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

    }
})();
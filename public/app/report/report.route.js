(function() {
    'use strict';

    angular
        .module('shinypocket.report')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('reports', {
                url: "/reports",
                authenticate: true,
                views: {
                    'content': { templateUrl: "app/report/report.template.html", controller: "ReportController as vm" }
                }
            })

            .state('reports.yearly', {
                url: "/yearly/:year?account&category&page",
                authenticate: true,
                params: {
                    page: '1',
                    squash: true
                },
                views: {
                    'content@': { templateUrl: "app/report/report.yearly.template.html", controller: "ReportYearlyController as vm" }
                }
            })

            .state('reports.yearlychart', {
                url: "/yearly/:year/pie",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/report/report.yearly.chart.template.html", controller: "ReportYearlyChartController as vm" }
                }
            })

            .state('reports.monthly', {
                url: "/monthly/:year/:month?account&category&page",
                authenticate: true,
                params: {
                    page: '1',
                    squash: true
                },
                views: {
                    'content@': { templateUrl: "app/report/report.monthly.template.html", controller: "ReportMonthlyController as vm" }
                }
            })

            .state('reports.monthlychart', {
                url: "/monthly/:year/:month/pie",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/report/report.monthly.chart.template.html", controller: "ReportMonthlyChartController as vm" }
                }
            })


    }

})();
(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('userService', userService);

    function userService($resource) {

        return {
            users: serviceUsers
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceUsers() {
            return $resource('/api/v1/users/me', {}, {
                update: {
                    method: 'PUT'
                }
            });
        }
    }

})();
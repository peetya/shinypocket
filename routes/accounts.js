var express = require('express');
var router  = express.Router();
var jwt     = require('jsonwebtoken');

var config  = require('../config/config');
var Account = require('../models/account');
var Verify  = require('../helpers/verify');

router.route('/')
    .get(Verify.verifyUser, function(req, res) {

        Account.find({ user: req.decoded.id }, function(err, accounts) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!accounts) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: 'Not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: accounts
            });
        });

    })

    .post(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Account.create(req.body, function(err, createdAccount) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            var id = createdAccount._id;

            Account.findById(id, function(err, account) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                if(!account) {
                    return res.status(404).json({
                        success: false,
                        code: 404,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: id + ' not found'
                        }
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 201,
                    data: account
                });
            });

        });

    });

router.route('/:id')
    .get(Verify.verifyUser, function(req, res) {

        Account.findById(req.params.id, function(err, account) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!account) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: account
            });
        });

    })

    .put(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Account.findByIdAndUpdate(req.params.id, { $set: req.body }, function(err, account) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!account) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: account
            });
        });

    })

    .delete(Verify.verifyUser, function(req, res) {

        Account.findOneAndRemove({ _id: req.params.id, user: req.decoded.id }, function(err, account) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!account) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: account
            });
        });

    });

module.exports = router;
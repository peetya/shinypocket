var express  = require('express');
var router   = express.Router();
var jwt      = require('jsonwebtoken');
var mongoose = require('mongoose');

var config   = require('../config/config');
var Cashflow = require('../models/cashflow');
var User     = require('../models/user');
var Verify   = require('../helpers/verify');

router.route('/:year')
    .get(Verify.verifyUser, function(req, res) {

        var pagination = {};

        var currentYear = req.params.year;
        var nextYear    = (parseInt(req.params.year) + 1).toString();
        var dateStart   = new Date(currentYear + "-01-01T00:00:00.000Z");
        var dateEnd     = new Date(nextYear + "-01-01T00:00:00.000Z");
        var query       = Cashflow.find({ user: req.decoded.id, date: { "$gte": dateStart, "$lt": dateEnd } });
        var queryTotal  = Cashflow.find({ user: req.decoded.id, date: { "$gte": dateStart, "$lt": dateEnd } });

        if(req.query.category) {
            query.find({ category: req.query.category });
            queryTotal.find({ category: req.query.category });
        }

        if(req.query.account) {
            query.find({ account: req.query.account });
            queryTotal.find({ account: req.query.account });
        }

        query.populate('account');
        query.populate('category');

        query.sort([['date', 'descending']]);

        queryTotal.count().exec(function(err, total) {
            if (err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            pagination.numberOfItems = total;

            if (req.query.page && req.query.limit) {
                pagination.limit = parseInt(req.query.limit);
                pagination.current = parseInt(req.query.page);

                if (pagination.numberOfItems > 0) {
                    pagination.numberOfPages = Math.ceil(pagination.numberOfItems / pagination.limit);
                } else {
                    pagination.numberOfPages = 0;
                }

                if ((pagination.current + 1) <= pagination.numberOfPages)
                    pagination.next = pagination.current + 1;

                if ((pagination.current - 1) > 0)
                    pagination.prev = pagination.current - 1;

                query.limit(pagination.limit);
                query.skip((pagination.current - 1) * pagination.limit);
            }

            query.exec(function(err, cashflows) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: cashflows,
                    pagination: pagination
                });
            });

        });

    });

router.route('/:year/:month')
    .get(Verify.verifyUser, function(req, res) {

        var pagination = {};
        var currentMonth = "0" + req.params.month;
        var nextMonth    = "0" + (parseInt(req.params.month) + 1).toString();
        var nextYear     = req.params.year;

        // TODO: ide valami szebb megoldás kéne :)
        if(nextMonth == '013') {
            nextYear  = (parseInt(nextYear) + 1).toString();
            nextMonth = '01';
        }

        var dateStart    = new Date(req.params.year + "-" + currentMonth.substr(currentMonth.length - 2) + "-01T00:00:00.000Z");
        var dateEnd      = new Date(nextYear + "-" + nextMonth.substr(nextMonth.length - 2) + "-01T00:00:00.000Z");
        var query        = Cashflow.find({ user: req.decoded.id, date: { "$gte": dateStart, "$lt": dateEnd } });
        var queryTotal   = Cashflow.find({ user: req.decoded.id, date: { "$gte": dateStart, "$lt": dateEnd } });

        if(req.query.category) {
            query.find({ category: req.query.category });
            queryTotal.find({ category: req.query.category });
        }

        if(req.query.account) {
            query.find({ account: req.query.account });
            queryTotal.find({ account: req.query.account });
        }

        query.populate('account');
        query.populate('category');

        query.sort([['date', 'descending']]);

        queryTotal.count().exec(function(err, total) {
            if (err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            pagination.numberOfItems = total;

            if (req.query.page && req.query.limit) {
                pagination.limit = parseInt(req.query.limit);
                pagination.current = parseInt(req.query.page);

                if (pagination.numberOfItems > 0) {
                    pagination.numberOfPages = Math.ceil(pagination.numberOfItems / pagination.limit);
                } else {
                    pagination.numberOfPages = 0;
                }

                if ((pagination.current + 1) <= pagination.numberOfPages)
                    pagination.next = pagination.current + 1;

                if ((pagination.current - 1) > 0)
                    pagination.prev = pagination.current - 1;

                query.limit(pagination.limit);
                query.skip((pagination.current - 1) * pagination.limit);
            }

            query.exec(function(err, cashflows) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 200,
                    data: cashflows,
                    pagination: pagination
                });
            });

        });

    });

module.exports = router;
(function () {
    'use strict';

    angular
        .module('shinypocket.core')
        .factory('AuthInterceptor', AuthInterceptor);

    function AuthInterceptor($q, $injector) {

        return {
            'request': function(config) {
                config.headers = config.headers || {};
                if (window.localStorage.getItem('authToken')) {
                    $injector.get('$http').defaults.headers.common.Authorization = window.localStorage.getItem('authToken');
                }

                return config;
            },

            'responseError': function(response) {
                if (response.status === 401) {
                    $injector.get('$state').go('login');
                }

                return $q.reject(response);
            }
        };

    }

})();
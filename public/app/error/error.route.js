(function() {
    'use strict';

    angular
        .module('shinypocket.error')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('error', {
                url: "/error/:code",
                views: {
                    'content': { templateUrl: "app/error/error.template.html", controller: "ErrorController as vm" }
                }
            })

    }

})();
(function() {
    'use strict';

    angular
        .module('shinypocket.login')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('login', {
                url: "/login",
                views: {
                    'content': { templateUrl: "app/login/login.template.html", controller: "LoginController as vm" }
                }
            })

    }

})();
(function() {
    'use strict';

    angular
        .module('shinypocket.logger')
        .factory('logger', logger);

    logger.$inject = ['$log', 'toastr'];

    function logger($log, toastr) {
        var service = {
            showToasts: true,

            error   : error,
            info    : info,
            success : success,
            warning : warning,

            log     : $log.log
        };

        return service;
        /////////////////////

        function error(message, title, data) {
            toastr.error(message, title);
            $log.error('Error: ' + message, data);
        }

        function info(message, title, data) {
            toastr.info(message, title);
            $log.info('Info: ' + message, data);
        }

        function success(message, title, data) {
            toastr.success(message, title);
            $log.info('Success: ' + message, data);
        }

        function warning(message, title, data) {
            toastr.warning(message, title);
            $log.warn('Warning: ' + message, data);
        }
    }
}());
var express    = require('express');
var cors       = require('cors');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');

var config     = require('./config/config');

var authRouter     = require('./routes/auth');
var userRouter     = require('./routes/users');
var accountRouter  = require('./routes/accounts');
var categoryRouter = require('./routes/categories');
var cashflowRouter = require('./routes/cashflows');
var reportRouter   = require('./routes/reports');
var chartRouter   = require('./routes/charts');

var app   = express();
var mongo = process.env.VCAP_SERVICES;
var port  = process.env.PORT || 3000;

// Set up CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "X-ACCESS_TOKEN, Access-Control-Allow-Origin, Authorization, Origin, x-requested-with, Content-Type, Content-Range, Content-Disposition, Content-Description");
    next();
});

// Static
app.use(express.static(__dirname + '/public'));

// MongoDB
var conn_str = "";
mongoose.Promise = global.Promise;
if (mongo) {
    var env = JSON.parse(mongo);
    if (env['mongodb']) {
        mongo = env['mongodb'][0]['credentials'];
        if (mongo.url) {
            conn_str = mongo.url;
        } else {
            console.log("No mongo found");
        }
    } else {
        conn_str = 'mongodb://localhost:27017';
    }
} else {
    conn_str = 'mongodb://localhost:27017';
    //conn_str = 'mongodb://localhost:27017/shinypocket';
}
mongoose.connect(conn_str);

// BodyParser
app.use(bodyParser.urlencoded({'extended':'true'}));
app.use(bodyParser.json());

// Routes
app.use('/api/v1/auth', authRouter);
app.use('/api/v1/users', userRouter);
app.use('/api/v1/accounts', accountRouter);
app.use('/api/v1/categories', categoryRouter);
app.use('/api/v1/cashflows', cashflowRouter);
app.use('/api/v1/reports', reportRouter);
app.use('/api/v1/charts', chartRouter);

// Portal routes
app.all('/*', function(req, res) {
    res.sendFile('public/index.html', { root: __dirname });
});

app.listen(port, function() {
    console.log('ShinyPocket app listening on port 3000');
});
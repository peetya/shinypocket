(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('category', {
                url: "/categories",
                authenticate: true,
                views: {
                    'content': { templateUrl: "app/category/category.template.html", controller: "CategoryController as vm" }
                }
            })

            .state('category.new', {
                url: "/new",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/category/category.form.template.html", controller: "CategoryNewController as vm" }
                }
            })

            .state('category.edit', {
                url: "/edit/:id",
                authenticate: true,
                views: {
                    'content@': { templateUrl: "app/category/category.form.template.html", controller: "CategoryEditController as vm" }
                }
            })

    }

})();
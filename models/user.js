var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var Schema   = mongoose.Schema;

var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true
    },
    currency: {
        type: String,
        required: true,
        default: 'EUR'
    }
}, {
    timestamps: true
});

UserSchema.pre('save', function(next) {
    var user = this;

    if(this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err, salt) {
            if(err) return next(err);

            bcrypt.hash(user.password, salt, function() {}, function(err, hash) {
                if (err) return next(err);

                user.password = hash;
                next();
            });
        })
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function(pw, cb) {
    bcrypt.compare(pw, this.password, function(err, isMatch) {
        if (err) return cb(err);

        cb(null, isMatch);
    })
};

module.exports = mongoose.model('User', UserSchema);
(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .config(interceptorConfig);

    function interceptorConfig($httpProvider) {

        $httpProvider.interceptors.push("AuthInterceptor");

    }

})();
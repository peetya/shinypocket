var express  = require('express');
var router   = express.Router();
var jwt      = require('jsonwebtoken');

var config   = require('../config/config');
var Cashflow = require('../models/cashflow');
var Verify   = require('../helpers/verify');

router.route('/')
    .post(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Cashflow.create(req.body, function(err, createdCashflow) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            var id = createdCashflow._id;

            Cashflow.findById(id, function(err, cashflow) {
                if(err) {
                    return res.status(403).json({
                        success: false,
                        code: 403,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: err.message
                        }
                    });
                }

                if(!cashflow) {
                    return res.status(404).json({
                        success: false,
                        code: 404,
                        data: {
                            path: req.route.path,
                            url: req.originalUrl,
                            method: req.method,
                            message: id + ' not found'
                        }
                    });
                }

                res.status(200).json({
                    success: true,
                    code: 201,
                    data: cashflow
                });
            });

        });

    });

router.route('/:id')
    .get(Verify.verifyUser, function(req, res) {

        Cashflow.findById(req.params.id, function(err, cashflow) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!cashflow) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: cashflow
            });
        });

    })

    .put(Verify.verifyUser, function(req, res) {

        req.body.user = req.decoded.id;

        Cashflow.findByIdAndUpdate(req.params.id, { $set: req.body }, function(err, cashflow) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!cashflow) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: cashflow
            });
        });

    })

    .delete(Verify.verifyUser, function(req, res) {

        Cashflow.findOneAndRemove({ _id: req.params.id, user: req.decoded.id }, function(err, cashflow) {
            if(err) {
                return res.status(403).json({
                    success: false,
                    code: 403,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: err.message
                    }
                });
            }

            if(!cashflow) {
                return res.status(404).json({
                    success: false,
                    code: 404,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: req.params.id + ' not found'
                    }
                });
            }

            res.status(200).json({
                success: true,
                code: 200,
                data: cashflow
            });
        });

    });

module.exports = router;
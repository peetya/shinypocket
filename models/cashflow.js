var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var CashflowSchema = new Schema({
    amount: {
        type: Number,
        required: true
    },
    note: {
        type: String
    },
    date: {
        type: Date,
        required: true
    },
    type: {
        type: String,
        required: true,
        enum: ['expense', 'income']
    },
    account: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Account',
        required: true
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true
    }

}, {
    timestamps: true
});

module.exports = mongoose.model('Cashflow', CashflowSchema);
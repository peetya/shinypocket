(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .controller('CategoryEditController', CategoryEditController);

    CategoryEditController.$inject = ['$q', '$state', '$stateParams', 'logger', 'categoryService', 'iconService'];

    function CategoryEditController($q, $state, $stateParams, logger, categoryService, iconService) {

        var vm   = this;

        vm.category  = {};
        vm.title     = 'Edit Category';
        vm.icons     = iconService.icons;

        vm.save      = save;

        activate();

        function activate() {

            var promises = [getCategory()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function save() {
            var data = {
                name: vm.category.name,
                icon: vm.category.icon
            };
            categoryService.categories().update({id: $stateParams.id}, data, function(category) {

                logger.success('Category successfully modified!');
                $state.go('category');

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

        function getCategory() {
            var deferrer = $q.defer();

            categoryService.categories().get({id: $stateParams.id}, function(category) {

                deferrer.resolve();
                vm.category = category.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .controller('AccountNewController', AccountNewController);

    AccountNewController.$inject = ['$state', 'logger', 'accountService', 'iconService'];

    function AccountNewController($state, logger, accountService, iconService) {

        var vm   = this;

        vm.account  = {};
        vm.title    = 'Add Account';
        vm.icons    = iconService.icons;

        vm.save      = save;

        ////////// Controller functions //////////

        function save() {
            accountService.accounts().save(vm.account, function(account) {

                logger.success('Account successfully created!');
                $state.go('account');

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

    }
})();
(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('categoryService', categoryService);

    function categoryService($resource) {

        return {
            categories: serviceCategories
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceCategories() {
            return $resource('/api/v1/categories/:id', { id: '@id' }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    }

})();
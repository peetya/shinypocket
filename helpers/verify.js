var jwt    = require('jsonwebtoken');

var config = require('../config/config');
var User   = require('../models/user');

exports.verifyUser = function(req, res, next) {
    var token = req.headers['authorization'];

    if(token) {

        var token = token.split(' ')[1];
        jwt.verify(token, config.secret, function(err, decoded) {
            if(err) {
                return res.status(401).json({
                    success: false,
                    code: 401,
                    data: {
                        path: req.route.path,
                        url: req.originalUrl,
                        method: req.method,
                        message: 'Not authenticated'
                    }
                });
            }

            req.decoded = decoded;
            next();
        });

    } else {

        return res.status(403).json({
            success: false,
            code: 403,
            data: {
                path: req.route.path,
                url: req.originalUrl,
                method: req.method,
                message: 'Please log in'
            }
        });

    }
};
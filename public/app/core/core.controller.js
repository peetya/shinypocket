(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .controller('MainController', MainController);

    MainController.$inject = ['$scope', '$q', '$uibModal', '$window', '$state', 'authService', 'userService', 'logger'];

    function MainController($scope, $q, $uibModal, $window, $state, authService, userService, logger) {

        var vm = this;

        vm.isAuthenticated = false;
        vm.navCollapsed    = true;
        vm.currentYear     = new Date().getFullYear();
        vm.currentMonth    = new Date().getMonth() + 1;

        vm.loggedInUser    = {};
        vm.friendRequests  = {};

        vm.logout              = logout;
        vm.openAddExpenseModal = openAddExpenseModal;
        vm.openAddIncomeModal  = openAddIncomeModal;

        activate();

        function activate() {
            vm.isAuthenticated = authService.isAuthenticated();

            if (vm.isAuthenticated) {
                var promises = [getLoggedInUser()];

                return $q.all(promises).then(function() {

                    // do nothing...

                }, function() {

                    //TODO: redirect to 500
                    $state.go('home');

                });
            }

        }

        ////////////

        function getLoggedInUser() {
            var deferrer = $q.defer();

            userService.users().get({ username: 'me' }, function(user) {

                deferrer.resolve();
                vm.loggedInUser = user.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function logout() {
            authService.destroyUserCredentials();
            $window.location.replace("/");
        }

        function openAddExpenseModal() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/cashflow/cashflow.modal.template.html',
                controller: 'ExpenseController as vm',
                scope: $scope
                //size: 'lg'
            });
        }

        function openAddIncomeModal() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'app/cashflow/cashflow.modal.template.html',
                controller: 'IncomeController as vm',
                scope: $scope
                //size: 'lg'
            });
        }

    }
})();
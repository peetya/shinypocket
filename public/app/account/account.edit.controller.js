(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .controller('AccountEditController', AccountEditController);

    AccountEditController.$inject = ['$q', '$state', '$stateParams', 'logger', 'accountService', 'iconService'];

    function AccountEditController($q, $state, $stateParams, logger, accountService, iconService) {

        var vm   = this;

        vm.account  = {};
        vm.title    = 'Edit Account';
        vm.icons    = iconService.icons;

        vm.save      = save;

        activate();

        function activate() {

            var promises = [getAccount()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function save() {
            var data = {
                name: vm.account.name,
                icon: vm.account.icon
            };
            accountService.accounts().update({id: $stateParams.id}, data, function(account) {

                logger.success('Account successfully modified!');
                $state.go('account');

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

        function getAccount() {
            var deferrer = $q.defer();

            accountService.accounts().get({id: $stateParams.id}, function(account) {

                deferrer.resolve();
                vm.account = account.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

    }
})();